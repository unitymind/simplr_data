module SimplrData
  class Engine < ::Rails::Engine
    isolate_namespace SimplrData

    config.generators do |g|
      g.test_framework :rspec
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
    end

    initializer :append_migrations do |app|
      unless app.root.to_s.match root.to_s
        config.paths['db/migrate'].expanded.each do |expanded_path|
          app.config.paths['db/migrate'] << expanded_path
        end
      end
    end

    initializer :append_factories do |app|
      unless app.root.to_s.match root.to_s
        ::FactoryGirl.definition_file_paths << File.join(config.root, 'spec', 'factories') if Rails.env.test?
      end
    end
  end
end
