require 'ledermann-rails-settings'

module SimplrData
  class Consumer < ActiveRecord::Base
    has_many :oauth2_tokens
    has_settings :oauth2

    validates_presence_of :identity
    validates_uniqueness_of :identity
  end
end
