module SimplrData
  class Oauth2Token < ActiveRecord::Base
    belongs_to :user

    validates_presence_of :user

    include ::SimplrData::Concerns::Oauth2Token::Identity
  end
end
