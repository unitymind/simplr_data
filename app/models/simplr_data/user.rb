module SimplrData
  class User < ActiveRecord::Base
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :invitable, :database_authenticatable, :registerable, :confirmable,
           :lockable, :recoverable, :rememberable, :trackable, :validatable, :invitable

    include ::SimplrData::Concerns::User::Roles
    include ::SimplrData::Concerns::User::Validations
    include ::SimplrData::Concerns::User::Profiles
    include ::SimplrData::Concerns::User::Oauth2
    include ::SimplrData::Concerns::User::Invites
  end
end
