module SimplrData
  module Google
    module Analytics
      class Account
        TTL = 30.minutes

        include ::Mongoid::Document
        include ::Mongoid::Timestamps

        field :account_id, type: String
        field :owner_id, type: Integer
        field :name, type: String

        has_many :web_properties, class_name: 'SimplrData::Google::Analytics::WebProperty'

        scope :by_owner, ->(owner_id) { where(owner_id: owner_id) }

        def expired?
          (self.updated_at + TTL) < Time.now.utc
        end

        def self.expired_for?(owner_id)
          by_owner(owner_id).map(&:expired?).include?(false)
        end
      end
    end
  end
end