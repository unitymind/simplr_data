module SimplrData
  module Google
    module Analytics
      class Profile
        include ::Mongoid::Document

        field :internal_id, type: String
        field :name, type: String
        field :type, type: String
        field :assigned_to, type: Integer
        field :currency, type: String
        field :url, type: String

        belongs_to :web_property, class_name: 'SimplrData::Google::Analytics::WebProperty'
      end
    end
  end
end