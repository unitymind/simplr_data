module SimplrData
  module Google
    module Analytics
      class WebProperty
        include ::Mongoid::Document

        field :public_id, type: String
        field :internal_id, type: String
        field :name, type: String
        field :level, type: String

        belongs_to :account, class_name: 'SimplrData::Google::Analytics::Account'
        has_many :profiles, class_name: 'SimplrData::Google::Analytics::Profile'
      end
    end
  end
end