module SimplrData
  class Job
    include ::Mongoid::Document
    include ::Mongoid::Timestamps

    field :owner_id, type: Integer
    field :jid, type: String
    field :performed_by, type: String
    field :args, type: Array
    field :status, type: Symbol
  end
end