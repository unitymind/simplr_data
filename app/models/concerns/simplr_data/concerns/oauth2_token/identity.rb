require 'digest'
require 'jwt'

module SimplrData
  module Concerns
    module Oauth2Token
      module Identity
        extend ActiveSupport::Concern

        module ClassMethods
          def generate_digest(user_id, consumer_id)
            Digest::SHA256.hexdigest([user_id, consumer_id, DateTime.now.to_time.to_f].flatten.map(&:to_s).join('|'))
          end
        end

        included do
          attr_readonly :user_id, :consumer_id, :digest
          belongs_to :consumer

          validates_presence_of :consumer

          validates_uniqueness_of :provider_user_id, scope: [:user_id, :consumer_id],
                                  unless: Proc.new { |token| token.provider_user_id.blank? }

          validates_presence_of :provider_user_id, unless: Proc.new { |token| token.access_token.blank? }

          before_validation do |token|
            unless token.decoded_id_token.blank?
              token.provider_user_id = token.decoded_id_token['id']
            end
          end

          before_create do |token|
            token.digest = self.class.generate_digest(token.user_id, token.consumer_id)
          end

          scope :by_consumer_identity, ->(identity) { joins(:consumer).where('simplr_data_consumers.identity' => identity) }
        end

        def is_active?
          ! self.access_token.blank?
        end

        def expired?
          self.expires_at <  DateTime.now
        end

        def can_refresh?
          ! self.refresh_token.blank?
        end

        def decoded_id_token
          if self.id_token.blank?
            {}
          else
            JWT.decode(self.id_token, nil, false)[0]
          end
        end
      end
    end
  end
end