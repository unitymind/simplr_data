module SimplrData
  module Concerns
    module User
      module Validations
        extend ActiveSupport::Concern

        included do
          validates :email, email_format: { message: I18n.t('errors.registrations.invalid_email') }
          validates_acceptance_of :tos_agreement, allow_nil: false, accept: true, on: :create
        end
      end
    end
  end
end