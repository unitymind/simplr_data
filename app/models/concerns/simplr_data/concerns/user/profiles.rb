module SimplrData
  module Concerns
    module User
      module Profiles
        extend ActiveSupport::Concern

        included do
          has_one :profile

          after_create do |user|
            user.create_profile! if user.profile.nil?
          end
        end
      end
    end
  end
end