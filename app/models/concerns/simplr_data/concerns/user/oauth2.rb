module SimplrData
  module Concerns
    module User
      module Oauth2
        extend ActiveSupport::Concern

        included do
          has_many :oauth2_tokens
        end

        def connected_to?(identity)
          self.oauth2_tokens.by_consumer_identity(identity).exists?
        end
      end
    end
  end
end