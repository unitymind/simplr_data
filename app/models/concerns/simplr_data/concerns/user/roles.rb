module SimplrData
  module Concerns
    module User
      module Roles
        extend ActiveSupport::Concern

        included do
          enum role: {
              customer: 0,
              agency: 1,
              manager: 2,
              admin: 3
          }

          after_initialize :set_default_role, if: :new_record?
        end

        def set_default_role
          self.role ||= :customer
        end
      end
    end
  end
end