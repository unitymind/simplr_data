module SimplrData
  module Concerns
    module User
      module Invites
        extend ActiveSupport::Concern

        included do
          has_many :invited_users, class_name: SimplrData::User, foreign_key: :invited_by_id
        end

      end
    end
  end
end