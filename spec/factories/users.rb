# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user, class: SimplrData::User do
    email { Faker::Internet.safe_email }
    password { Faker::Internet.password(8, 128) }
    password_confirmation { password }
    tos_agreement { true }
  end
end
