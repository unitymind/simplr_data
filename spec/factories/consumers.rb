# Read about factories at https://github.com/thoughtbot/factory_girl

# FactoryGirl.define do
#   factory :google_analytics_consumer, class: SimplrData::Consumer do
#     identity { 'google.analytics.reader' }
#     title { 'Google Analytics Reader' }
#
#     after(:create) do |consumer|
#       consumer.settings(:oauth2).update_attributes!({
#         provider: 'google',
#         redirect_uri: "#{ENV['FRONTEND_HOST']}/authentication/oauth2/google",
#         scopes: 'https://www.googleapis.com/auth/analytics.readonly email'
#       })
#     end
#   end
# end
