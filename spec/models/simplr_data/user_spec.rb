require 'rails_helper'

module SimplrData
  RSpec.describe User, type: :model do
    describe 'validations' do
      describe 'tos_agreement' do
        context 'invalid' do
          it { expect(build(:user, tos_agreement: false)).to_not be_valid }
        end

        context 'valid' do
          it { expect(build(:user, tos_agreement: true)).to be_valid }
        end
      end
    end

    describe 'profile' do
      it 'exists only after create' do
        u = build(:user)
        u.skip_confirmation!
        expect(u.profile).to be_nil
        u.save
        expect(u.profile).to_not be_nil
        expect(u.profile.user).to eql u
      end
    end
  end
end
