$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'simplr_data/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'simplr_data'
  s.version     = SimplrData::VERSION
  s.authors     = ['Vitaliy V. Shopov']
  s.email       = ['vitaliy.shopov@cleawing.com']
  s.homepage    = 'http://simplr.ru'
  s.summary     = 'Shared models, packed as engine.'
  s.description = 'Shared models, packed as engine.'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'spec/factories/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']

  s.add_dependency 'rails', '~> 4.1.6'
  s.add_dependency 'validates_email_format_of', '~> 1.6.0'
  s.add_dependency 'devise', '~> 3.2.4'
  s.add_dependency 'devise_invitable', '~> 1.3.6'
  s.add_dependency 'ledermann-rails-settings', '~> 2.3.0'
  s.add_dependency 'jwt', '~> 1.0.0'
  s.add_dependency 'mongoid', '~> 4.0.0'
  s.add_dependency 'activerecord-jdbcpostgresql-adapter'

  s.add_development_dependency 'rspec-rails', '~> 3.1.0'
  s.add_development_dependency 'factory_girl_rails', '~> 4.4.1'
  s.add_development_dependency 'faker', '~> 1.4.2'
  s.add_development_dependency 'database_cleaner', '~> 1.3.0'
end
