class RemoveRoleFromProfile < ActiveRecord::Migration
  def change
    remove_column :simplr_data_profiles, :role_id
  end
end
