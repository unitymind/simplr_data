class CreateSimplrDataOauth2Tokens < ActiveRecord::Migration
  def change
    create_table :simplr_data_oauth2_tokens do |t|
      t.references :user, null: false, index: true
      t.references :consumer, null: false, index: true
      t.string :state
      t.string :digest, null: false, index: true
      t.text :id_token
      t.string :provider_user_id
      t.string :access_token
      t.string :refresh_token
      t.datetime :expires_at
      t.timestamps
    end
  end
end
