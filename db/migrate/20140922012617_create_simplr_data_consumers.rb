class CreateSimplrDataConsumers < ActiveRecord::Migration
  def change
    create_table :simplr_data_consumers do |t|
      t.string :identity, null: false
      t.string :title, null: false
      t.timestamps
    end
    add_index :simplr_data_consumers, :identity, unique: true
  end
end
