class AddTosAgreementToUser < ActiveRecord::Migration
  def change
    add_column :simplr_data_users, :tos_agreement, :boolean, null: false, default: false
  end
end
