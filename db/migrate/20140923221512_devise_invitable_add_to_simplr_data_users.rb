class DeviseInvitableAddToSimplrDataUsers < ActiveRecord::Migration
  def change
    add_column :simplr_data_users, :invitation_token, :string
    add_column :simplr_data_users, :invitation_created_at, :datetime
    add_column :simplr_data_users, :invitation_sent_at, :datetime
    add_column :simplr_data_users, :invitation_accepted_at, :datetime
    add_column :simplr_data_users, :invitation_limit, :integer
    add_column :simplr_data_users, :invited_by_id, :integer
    add_column :simplr_data_users, :invited_by_type, :string
    add_index  :simplr_data_users, :invitation_token, unique: true

    change_column :simplr_data_users, :encrypted_password, :string, null: true
  end
end