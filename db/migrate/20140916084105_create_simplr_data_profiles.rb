class CreateSimplrDataProfiles < ActiveRecord::Migration
  def change
    create_table :simplr_data_profiles do |t|
      t.references :user, index: true, null: false
      t.references :role, index: true, null: false

      t.string :first_name
      t.string :last_name
      t.string :appointment

      t.timestamps
    end
  end
end
